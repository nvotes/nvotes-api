import React from "react"
import { Breadcrumbs, BreadcrumbItem } from "../Breadcrumbs/Breadcrumbs"
import { useTranslate } from 'react-admin'
import { shortUuid } from "../UuidInput/UuidInput"

export const Item = (props: any) => {
  const { name, label = undefined, pathOptions } = props
  const translate = useTranslate()
  const newProps = {
    ...props,
    label: label === undefined 
      ? translate(`nvotes.gui.menu.${name}`) 
      : typeof label === 'string'
        ? label
        : label(pathOptions)
  }
  return <BreadcrumbItem {...newProps} />
}

export const AppBreadcrumbs = () => {
  const translate = useTranslate()
  const listItem = <Item key="list" name="list"></Item>
  const createItem = <Item key="create" name="create"></Item>
  const editItem = <Item 
    key="edit"
    name="edit" 
    label={(pathOptions: any) => 
      translate(`nvotes.gui.menu.edit`, {id: shortUuid(pathOptions.id)})
    }
  />
  const deleteItem = <Item key="delete" label="delete"></Item>
  const crudItems = [
    listItem,
    createItem,
    editItem,
    deleteItem
  ]

  return (
    <Breadcrumbs>
      <Item key="organizations" name="organizations">{crudItems}</Item>
      <Item key="users" name="users">{crudItems}</Item>
    </Breadcrumbs>
  )
}

export default AppBreadcrumbs
