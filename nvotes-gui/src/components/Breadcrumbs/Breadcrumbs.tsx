import React, { useContext, useMemo } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Link, 
  Typography
} from '@material-ui/core'
import { AppContext } from '../../providers/AppContext'

const useStyles = makeStyles(
  theme => ({
    list: {
      listStyleType: 'none',
      padding: 0,
      margin: 0
    },
    listItem: {
      display: 'inline-block',
      margin: theme.spacing(1) + "px 0",
      '& a': {
        fontWeight: 'bold',
        fontSize: theme.spacing(4),
        color: theme.palette.primary.main,
        textTransform: 'capitalize'
      }
    },
    separator: {
      display: 'inline-block',
      margin: theme.spacing(1),
      fontWeight: 'bold',
      fontSize: theme.spacing(4),
      color: '#c4cacd'
    },
    last: {
      display: 'inline-block',
      fontWeight: 'bold',
      fontSize: theme.spacing(4),
      color: theme.palette.secondary.main,
      textTransform: 'capitalize'
    }
  }),
  { name: 'NvBreadcrumbs' }
)

const defaultTo = (pathArray: string[], pathOptions: any) => {
  return "/#/" + pathArray
    .map((path) => path[0].toUpperCase() +  path.slice(1, path.length-1))
    .join('/')
}

type LabelCallback = (pathOptions: any) => string

interface BreadcrumbItemWrapperProps {
  label: string | LabelCallback,
  path?: string,
  pathOptions?: any,
  index?: number,
  to?: (pathArray: string[], pathOptions: any) => string
}

export const BreadcrumbItemWrapper = (props: BreadcrumbItemWrapperProps) => {
  const { 
    label = "",
    path = "",
    pathOptions = undefined,
    index = 0,
    to = defaultTo
  } = props
  const classes = useStyles()
  const pathArray = path.split('.')
  const isLast = (pathArray.length === (index + 1))
  const url = to(pathArray.slice(0, index + 1), pathOptions)
  const generatedLabel = typeof label === 'string' ? label : label(pathOptions)
  return (
    !isLast
      ? <li className={classes.listItem}>
        <Link color="inherit" href={url}>
          {generatedLabel}
        </Link>
        <div className={classes.separator}>›</div>
      </li>
      : <li className={classes.listItem}>
        <Typography color="textPrimary" className={classes.last}>
          {generatedLabel}
        </Typography>
      </li>
  )
}

interface BreadcrumbsItemProps {
  name: string,
  label?: string | LabelCallback,
  children?: React.ReactElement[] | React.ReactElement,
  wrapper?: React.ReactElement,
  path?: string,
  pathOptions?: any,
  index?: number,
  to?: (pathArray: string[], pathOptions: any) => string
}

export const BreadcrumbItem = (props: BreadcrumbsItemProps) => {
  const { 
    name,
    label = name,
    to = defaultTo, 
    wrapper = <BreadcrumbItemWrapper 
      path = ""
      index = {0}
      label = "" 
      to = {to}
    />, 
    index = 0, 
    path = "", 
    pathOptions = undefined,
    children = []
  } = props

  const pathArray = path.split('.')
  const nextName = pathArray[index + 1]

  return (
    <React.Fragment>
      {wrapper && React.cloneElement(
        wrapper,
        {
          path: path,
          pathOptions: pathOptions,
          label: label,
          index: index,
        }
      )}

      {React.Children.map(children, (element) => 
        (nextName === element.props.name)
          ? React.cloneElement(
              element, 
              {
                path: path,
                pathOptions: pathOptions,
                index: index+1
              }
            )
          : null
      )}
    </React.Fragment>
  )
}

interface BreadcrumbsProps {
  children: React.ReactElement[] | React.ReactElement
}

export const Breadcrumbs = (props: BreadcrumbsProps) => {
  const { 
    children,
  } = props

  // TODO: move path data getters out so that Breadcrumbs component is 
  // generic, detached from this specific implementation
  const appContext = useContext(AppContext)
  const path = appContext.breadcrumbsPath
  const pathOptions = appContext.breadcrumbsOptions

  const classes = useStyles()
  const index = 0
  const nextName = path.split('.')[index]

  return useMemo(() => (
    <nav aria-label="breadcrumb">
      <ol className={classes.list}>
        {React.Children.map(children, (element) => 
          (nextName === element.props.name)
            ? React.cloneElement(
              element, 
              {
                path: path,
                whatever: "hola",
                pathOptions: pathOptions,
                index: 0
              }
            )
            : null
        )}
      </ol>
    </nav>
  ),
  // Disables warning, it says `children` is not included, but we do not support
  // mutating children anyway
  // eslint-disable-next-line react-hooks/exhaustive-deps
  [path, pathOptions, nextName, classes.list])
}

export default Breadcrumbs
