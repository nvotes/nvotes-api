import React from 'react'
import { createMuiTheme, ThemeOptions, Theme } from '@material-ui/core/styles'
import Cookies from 'universal-cookie'

export interface IAppContext {
  themeOptions: ThemeOptions,
  theme: Theme,
  locale: string,
  breadcrumbsPath: string,
  breadcrumbsOptions: any
}

export interface IAppContextActions {
  setLocale: (locale: string) => void,
  setThemeOptions: (themeOptions: ThemeOptions) => void
  setBreadcrumbs: (path: string, options?: any) => void
}

const initialThemeOptions: any = {
  palette: {
    primary: {
        main: '#2185d0',
    },
    secondary: {
        light: '#8d9599',
        main: '#303030',
        dark: '#001064',
        contrastText: '#fff',
    },
    background: {
        default: '#fcfcfe',
    },
  },
  shape: {
    borderRadius: 10,
  },
  sidebar: {
    width: 320,
    closedWidth: 70
  }
}

const initialContext: IAppContext = {
  themeOptions: initialThemeOptions,
  theme: (() => {
    let theme: any = createMuiTheme(initialThemeOptions)

    // we don't set theme overrides directly in initialThemeOptions so that
    // we can access the theme var inside the overrides
    theme.overrides = {
      RaTreeMenu: {
        main: {
          [theme.breakpoints.up('md')]: {
            marginTop: '0.5em'
          },
          marginLeft: theme.spacing(2.25)
        }
      },
      RaTreeCustomMenuItem: {
        menuItem: {
          '& .menuItemName': {
            color: '#3e3e3e',
          },
          '& svg': {
            color: '#c4cacd'
          },
          '&:hover': {
            background: 'none',
            '& svg': {
              color: theme.palette.primary.main
            },
            '& .menuItemName': {
              color: theme.palette.primary.main
            },
          }
        },
        openMenuItem: {
          '& .menuItemName': {
            color: theme.palette.primary.main
          },
          '& svg': {
            color: theme.palette.primary.main
          }
        }
      },
      RaMenuItemLink: {
        root: {
          color: '#3e3e3e',

          '&:hover': {
            color: theme.palette.primary.main,
            background: 'none',
            '& svg': {
              color: theme.palette.primary.main
            }
          }
        },
        active: {
          color: theme.palette.primary.main,
          '& svg': {
            color: theme.palette.primary.main
          }
        },
        icon: {
          color: '#c4cacd'
        }
      },
      RaDatagrid: {
        row: {
          '& td': {
            wordBreak: 'break-all'
          }
        }
      },
      MuiTableCell: {
        sizeSmall: {
          padding: theme.spacing(0.75)
        }
      }
    }
    return theme
  })(),
  locale: 'en',
  breadcrumbsPath: 'dashboard',
  breadcrumbsOptions: []
}

export const AppContext = 
  React.createContext<IAppContext & Partial<IAppContextActions>>(
    initialContext,
  )
AppContext.displayName = "AppContext"

class AppProvider extends React.PureComponent {
  state: IAppContext = initialContext
  cookies: Cookies = new Cookies()

  constructor(props: {}) {
    super(props)

    const localeCookie = this.cookies.get('locale')
    const locale = (
      localeCookie ||
      (navigator.languages && navigator.languages[0]) ||
      navigator.language ||
      'en-US'
    )

    this.state = {
      ...initialContext,
      locale: locale
    }

    this.setLocale = this.setLocale.bind(this)
    this.setThemeOptions = this.setThemeOptions.bind(this)
    this.setBreadcrumbs = this.setBreadcrumbs.bind(this)
  }

  setLocale(locale: string) {
    this.cookies.set(
      'locale',
      locale,
      {
        path: '/'
      }
    )
    this.setState({...this.state, locale: locale})
  }

  setThemeOptions(themeOptions: ThemeOptions) {
    this.setState({
      ...this.state,
      themeOptions: themeOptions,
      theme: createMuiTheme(themeOptions)
    })
  }

  setBreadcrumbs(path: string, options?: any) {
    // this conditional avoid recursive update loops on rendering
    if (path === this.state.breadcrumbsPath) {
      return
    }
    this.setState({
      ...this.state,
      breadcrumbsPath: path,
      breadcrumbsOptions: options
    })
  }

  render() {
    const context = this.state
    const actions: IAppContextActions = {
      setThemeOptions: this.setThemeOptions,
      setLocale: this.setLocale,
      setBreadcrumbs: this.setBreadcrumbs
    }

    return (
      <AppContext.Provider value={{ ...context, ...actions }}>
        {this.props.children}
      </AppContext.Provider>
    )
  }
}

export default AppProvider