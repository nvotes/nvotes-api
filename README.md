# nVotes api chart

This is a kubernetes helm chart to deploy nVotes online voting platform API. 

It deploys everything required, including a postgresql database in a high availability configuration.

# Installation

## Option 1: Microk8s installation and configuration

Microk8s is a lightweight Kubernetes distribution made by Canonical that allows
you to create your own cluster in a your own local machine(s). To use it,
first [install it](https://microk8s.io/docs). Then make sure your
command line user has permissions to run `microk8s`. If  your user has no 
permissions, `microk8s` will print out instructions on how to proceed.

Afterwards, you need to configure your user kubernetes credentials. To do so,
execute the following command:

```bash
mkdir $HOME/.kube/
# Source: https://github.com/GoogleContainerTools/skaffold/issues/3571#issuecomment-578566142
microk8s kubectl config view --raw > $HOME/.kube/config
```

You will also have to enable some plugins in `microk8s` to be able to deploy 
effectively `nvotes-api`:

```bash
sudo microk8s.enable helm3 registry storage dns metrics-server
```

## Option 2: Using a cloud provider like Google Cloud or Amazon

TODO

## Helm installation and configuration

Deployment is done using helm3. Please follow the instructions to 
[install it](https://helm.sh/docs/intro/install/).

Our helm chart has some dependencies. You need to first add the dependency
repositories:

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
```

Then download the chart dependencies:

```bash
cd k8s/
helm dependency update # or alternatively "helm dep up" for short
```

## Generating and pushing to the registry docker images

To deploy from sources, we need to generate some docker images that the helm
chart will use:
- an image containing hasura and the metadata and migrations, required
  to generate the database.
- an image containing the compiled rust source code.

We can generate these two images executing the following commands:

```bash
docker build -t nvotes-api-hasura -f ./Dockerfile .
docker build -t nvotes-api-actions -f ./nvotes-api-actions/Dockerfile ./nvotes-api-actions/
docker build -t nvotes-gui -f ./nvotes-gui/Dockerfile ./nvotes-gui/
```

This generates the docker images `nvotes-api-hasura` and `nvotes-api-actions` 
with the tag latest, which should appear if you execute `docker image ls`,
showing something like:

```bash
REPOSITORY             TAG            IMAGE ID        CREATED              SIZE
nvotes-api-actions     latest         edb90c983ac9    About a minute ago   18.9MB
nvotes-api-hasura      latest         993f0e2cf213    4 minutes ago        131MB
```

To enable the usage of these images during deployment with helm, we need to
push them to the registry created by the helm docker registry plugin:

```bash
docker tag nvotes-api-hasura:latest localhost:32000/nvotes-api-hasura:latest
docker push localhost:32000/nvotes-api-hasura:latest
docker tag nvotes-api-actions:latest localhost:32000/nvotes-api-actions:latest
docker push localhost:32000/nvotes-api-actions:latest
docker tag nvotes-gui:latest localhost:32000/nvotes-gui:latest
docker push localhost:32000/nvotes-gui:latest
```

## Deployment with helm

Afterwards, you can deploy with helm:

```bash
helm install dev ./k8s
```

It will take some seconds to have all the containers up and working. You can 
see the status of the deployment with `kubectl get pods` which will
output something like the following when everything is working:

```
NAME                                               READY   STATUS    RESTARTS   AGE
dev-hasura-85cdcc694-h5zw8                         1/1     Running   1          1m
dev-kubeless-controller-manager-799df7668f-q58r5   3/3     Running   0          1m
dev-nvotes-api-actions-845f94466b-jh7pf            1/1     Running   0          1m
dev-postgresql-ha-pgpool-df6df7556-m5l7h           1/1     Running   0          1m
dev-postgresql-ha-postgresql-0                     1/1     Running   0          1m
```

## Connecting to Postgresql

To connect to the database using `microk8s`:

```bash
export POSTGRES_PASSWORD=$(kubectl get secret dev-postgresql-ha-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

kubectl run dev-postgresql-ha-client --rm --tty -i --restart='Never' --namespace default --image bitnami/postgresql:11 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql -h dev-postgresql-ha-pgpool -p 5432 -U postgres -d nvotesapi
```

To access to the hasura admin interface execute the following command that will
print the Hasura admin URL:

```bash
kubectl get endpoints dev-hasura -o jsonpath='http://{ .subsets[0].addresses[0].ip }:{.subsets[0].ports[0].port}{"\n"}'
```

## Upgrading a deployment

To update the helm chart if it's already deployed, simply run:

```bash
helm upgrade dev ./k8s
```

# Development

For each new version of the code, a new docker image needs to be built. This is
because the docker image will contain the code of the application, including
also the migrations and metadata for the database.

To facilitate software development we use [Skaffold](https://skaffold.dev/). 
Follow the instructions in https://skaffold.dev/docs/install/ to install it.

To start software development and once `skaffold` is installed, simply execute:

```bash
skaffold dev --default-repo="localhost:32000" --port-forward
```

This will launch locally in your microk8s kubernetes instance all the resources
to run `nvotes-api`, and will detect any source code changes and rebuild and
redeploy.

## Access to hasura console

To access the hasura console, if running through skaffold simply enter with
your web browser into http://localhost:8080 as it is automatically redirected 
to the hasura service.

Alternatively, you can lookup the address and port of the hasura (and access 
it) with the following command:

```bash
kubectl get endpoints dev-hasura -o jsonpath='http://{ .subsets[0].addresses[0].ip }:{.subsets[0].ports[0].port}{"\n"}'
```

# Access to nvotes gui

To access the nvotes user interface, if running through skaffold simply enter 
with your web browser into http://localhost:3000 as it is automatically 
redirected to nvotes gui.

Alternatively, you can lookup the address and port of the hasura (and access 
it) with the following command:

```bash
kubectl get endpoints dev-nvotes-gui -o jsonpath='http://{ .subsets[0].addresses[0].ip }:{.subsets[0].ports[0].port}{"\n"}'
```

# Run hasura CLI

To run hasura CLI, make sure you have installed the package dependencies with:

```bash
yarn
```

Once that is done, simple execute the `hasura` CLI with the `yarn` prefix. You
also need to configure the hasura endpoint because the IP address will change
in different deployments:

```bash
# only needed the first time in this session
export HASURA_GRAPHQL_ENDPOINT=$(kubectl get endpoints dev-hasura -o jsonpath='http://{ .subsets[0].addresses[0].ip }:{.subsets[0].ports[0].port}{"\n"}')

yarn hasura 
```

# Troubleshooting

## Development: Pgpool / Postgres not working, Name or service not known

If for some reason the pgpool pod stays not ready and is not able to connect
to postgresql, check that you can connect to the service directly via its
ip address:

```bash
$ kubectl get svc
NAME                                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
dev-postgresql-ha-pgpool                ClusterIP   10.152.183.81   <none>        5432/TCP   28m
dev-postgresql-ha-postgresql            ClusterIP   10.152.183.80   <none>        5432/TCP   28m
dev-postgresql-ha-postgresql-headless   ClusterIP   None            <none>        5432/TCP   28m
kubernetes                              ClusterIP   10.152.183.1    <none>        443/TCP    29h

# connecting to the database directly via its ip works
$ kubectl run dev-postgresql-ha-client --rm --tty -i --restart='Never' --image bitnami/postgresql:11 --env="PGPASSWORD=qwerty" --command -- psql -h 10.152.183.80 -p 5432 -U postgres -d repmgr
If you don't see a command prompt, try pressing enter.

repmgr=# select node_id, upstream_node_id, active, node_name, type from repmgr.nodes;
 node_id | upstream_node_id | active |           node_name            |  type   
---------+------------------+--------+--------------------------------+---------
    1000 |                  | t      | dev-postgresql-ha-postgresql-0 | primary
(1 row)

repmgr=# ^D\q

E0924 18:29:08.751940 3143878 v2.go:105] EOF
pod "dev-postgresql-ha-client" deleted

# connecting to the database via its service dns does not work?
$ kubectl run dev-postgresql-ha-client --rm --tty -i --restart='Never' --image bitnami/postgresql:11 --env="PGPASSWORD=qwerty" --command -- psql -h dev-postgresql-ha-postgresql -p 5432 -U postgres -d repmgr

psql: could not translate host name "dev-postgresql-ha-postgresql" to address: Name or service not known
pod "dev-postgresql-ha-client" deleted
pod default/dev-postgresql-ha-client terminated (Error)
```

This is an issue with the DNS resolver. This issue can be triggered by bad
iptables rules. Iptables rules are set when starting `microk8s`, but might be
messed by `NetworkManager` for example when reconnecting to a WiFi network.

To reset these iptables rules and solve this issue, you can execute:

```bash
microk8s stop
systemctl stop docker
iptables --flush
iptables -tnat --flush
systemctl start docker
microk8s start
```

You can recreate the pods or uninstall & install again the helm chart after that
and the issue should be solved. Find more information in this github issue: 
https://github.com/bitnami/charts/issues/2499#issuecomment-698468825 .

Once you get everything working, you should probably save the current rules to
be able to restore them later:

```bash
# to save the rules
iptables-save -f /etc/iptables/rules.v4

# to restore the rules
iptables-restore -n /etc/iptables/rules.v4
```

## Development: Manually garbage collecting docker images

When using skaffold for development, each time a change in code it creates a
new docker image. Sometimes these images are quite large so they can take some
space.

You can list all your docker images this way:

```bash
$ docker image ls --all
REPOSITORY                          TAG                        IMAGE ID            CREATED             SIZE
localhost:32000/nvotes-gui          836113d                    e601b1169fb5        18 minutes ago      781MB
<none>                              <none>                     704e4c374082        11 hours ago        131MB
localhost:32000/nvotes-api-hasura   a1f2bcf-dirty              9ec7db40c598        11 hours ago        131MB
<none>                              <none>                     e5f7e2ca3f0d        16 hours ago        779MB
<none>                              <none>                     3ed149b9af8a        16 hours ago        89.9MB
localhost:32000/nvotes-gui          dea3370                    7ab519304bee        2 days ago          781MB
<none>                              <none>                     3d50e035d6ca        2 days ago          89.3MB
<none>                              <none>                     67ae0a602e72        2 days ago          89.3MB
<none>                              <none>                     53ea9ff924b3        2 days ago          89.3MB
node                                12-alpine                  1f52b7199ba6        2 weeks ago         89.3MB
hasura/graphql-engine               v1.3.2.cli-migrations-v2   12e75692847a        3 weeks ago         131MB
ekidd/rust-musl-builder             stable                     b158ab05a3a9        3 weeks ago         1.44GB
hasura/graphql-engine               v1.3.1                     3ac6795af610        5 weeks ago         48.3MB
postgres                            12                         62473370e7ee        6 weeks ago         314MB
alpine                              latest                     a24bb4013296        4 months ago        5.57MB
```


Docker images consist of multiple layers. Dangling images are layers that have 
no relationship to any tagged images. They no longer serve a purpose and 
consume disk space. You can list them with:

```bash
docker images -f dangling=true
REPOSITORY                          TAG                        IMAGE ID            CREATED             SIZE
<none>                              <none>                     e5f7e2ca3f0d        16 hours ago        779MB
<none>                              <none>                     3ed149b9af8a        16 hours ago        89.9MB
<none>                              <none>                     3d50e035d6ca        2 days ago          89.3MB
<none>                              <none>                     67ae0a602e72        2 days ago          89.3MB
<none>                              <none>                     53ea9ff924b3        2 days ago          89.3MB
```

And remove them with `docker image prune`. However, skaffold images usually
have tags attached, but might just be too old. You can use `grep` to filter 
all images with repository `<none>` or `localhost:32000` older than 24 hours:

```bash
docker image ls | grep -e '^\(localhost\|<none\).*\(hours\|days\|weeks\|months\)'
REPOSITORY                          TAG                        IMAGE ID            CREATED             SIZE
localhost:32000/nvotes-gui          dea3370                    7ab519304bee        2 days ago          781MB
```

And use `awk` to get the image ids and `docker image rm` with `xargs` to remove
them:

```bash
docker image ls | grep -e '^\(localhost\|<none\).*\(hours\|days\|weeks\|months\)' | awk '{ print $3 }' | xargs docker image rm -f
```

## Patching an npm dependency in nvotes-gui

Sometimes we need to apply a small patch to a node_modules dependency in 
`nvotes-gui`. In  order to make it easy for the code to be up to date and not 
have to publish the  dependency in npm, you can use `patch-package`.

You will have to:
1. Git clone the original package repository.
2. Apply the patch to the source code.
3. Apply the patched source code and compiled to 
   `nvotes-gui/node_modules/<package-name>/` directory. The compiled directory 
   might be `lib/` or `esm/`.
4. Run `yarn patch-package <package-name>` to generate a patch in 
   `nvotes-gui/patches`.
5. (Probably) send a pull request to upstream to fix the issue.

Note that skaffold automatically detects changes in the `nvotes-gui/patches`
directory and automatically applies them.

More information: 
- https://www.npmjs.com/package/patch-package#benefits-of-patching-over-forking
- https://stackoverflow.com/questions/63675001/how-to-use-patch-package-to-patch-tsx-files
