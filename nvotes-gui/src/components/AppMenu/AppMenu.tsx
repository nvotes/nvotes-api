import * as React from "react"
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import SubscriptionsIcon from '@material-ui/icons/Subscriptions'
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle'
import Badge from '@material-ui/core/Badge'
import Fab from '@material-ui/core/Fab'
import classnames from 'classnames'
import TreeMenu from '@bb-tech/ra-treemenu'
import Sticky from 'react-stickynode'

import PopMenu from "../PopMenu/PopMenu"
import { useSelector } from "react-redux"
import { useTranslate } from 'react-admin'

const useStyles = makeStyles(
  theme => ({
    sticky: {
      top: theme.spacing(6.25)
    },
    treeMenu: {
      marginBottom: theme.spacing(15),
    },
    selector: {
      marginTop: theme.spacing(2.5),
      marginLeft: theme.spacing(4.5),
      marginBottom: theme.spacing(2),
      width: theme.spacing(31.25),
      borderRadius: theme.spacing(0.5),
      border: '2px solid #efefef',
      '&:hover': {
        borderColor: theme.palette.primary.main,
        background: 'inherit'
      }
    },
    collapsedIcon: {
      marginTop: theme.spacing(1),
      marginLeft: theme.spacing(2.5),
      marginBottom: theme.spacing(3.5),
      '& button': {
        color: '#c4cacd',
      },
      '&:hover button': {
        background: 'none',
        '& svg': {
          color: theme.palette.primary.main,
        }
      }
    },
    selectorIcon: {
      right: theme.spacing(5),
      position: 'fixed'
    },
    selectorText: {
      textTransform: 'none',
      overflowX: 'hidden',
      overflowY: 'hidden',
      textOverflow: 'ellipsis',
      marginRight: theme.spacing(2.5),
      fontWeight: 'normal',
      textAlign: 'left',
      display: 'block',
      width: '100%',
      whiteSpace: 'nowrap'
    },
    accountSetupWrapper: {
      background: theme.palette.primary.contrastText,
      width: '100%',
      maxWidth: theme.spacing(40),
      position: 'fixed',
      bottom: theme.spacing(4.5),
      left: '0'
    },
    accountSetupButton: {
      marginLeft: theme.spacing(4.5),
      padding: theme.spacing(3),
      textTransform: 'none'
    },
    accountSetupBadge: {
      '& > span.MuiBadge-badge': {
        padding: theme.spacing(2) + 'px ' + theme.spacing(1.5) + 'px',
        borderRadius: '50%',
        top: theme.spacing(0.75)
      }
    }
  })
)

const AppMenu = (props: any) => {
  const classes = useStyles()
  const open = useSelector((state: any) => state.admin.ui.sidebarOpen)
  const translate = useTranslate()

  return (
    <>
      <Sticky top={50}>
        <PopMenu
          label="nvotes.gui.menu.chooseElectoralProcess"
          className={classnames({[classes.collapsedIcon]: !open})}
          button={open
            ? <Button variant="outlined" className={classes.selector}>
                <span className={classes.selectorText}>
                  2020 Electoral Processes
                </span>
                <ExpandMoreIcon className={classes.selectorIcon} />
              </Button>
            : undefined
          }
          icon={open
            ? undefined
            : <SubscriptionsIcon />
          }
        >
          <div key="0"></div>
        </PopMenu>
        <TreeMenu {...props} className={classes.treeMenu} />
      </Sticky>

      {open && <div className={classes.accountSetupWrapper}>
          <Badge 
            color="error" 
            badgeContent="2"
            className={classes.accountSetupBadge}
          >
            <Fab 
              variant="extended"
              color="primary"
              aria-label="add"
              className={classes.accountSetupButton}
              >
              <PersonPinCircleIcon />
              {translate('nvotes.gui.menu.accountSetup')}
            </Fab>
          </Badge>
        </div>}
    </>
  )
}

export default AppMenu
