import React, { useContext } from "react"
import { 
  List, 
  Datagrid, 
  TextField, 
  DateField, 
  TextInput,
  Filter
} from 'react-admin'
import { AppContext } from "../../providers/AppContext"
import AppBreadcrumbs from "../../components/AppBreadcrumbs/AppBreadcrumbs"

const OrganizationFilter = (props: any) => (
  <Filter {...props}>
    <TextInput 
      label="Search" 
      source="id@_like,"
      alwaysOn />
  </Filter>
)

export const OrganizationList = (props: any) => {
  const appContext = useContext(AppContext)
  appContext.setBreadcrumbs?.('organizations.list', [])

  return (
    <>
      <AppBreadcrumbs />
      <List filters={<OrganizationFilter />} {...props}>
        <Datagrid rowClick="edit">
          <TextField source="id" />
          <DateField source="created_at" />
          <DateField source="updated_at" />
          <TextField source="name" />
        </Datagrid>
      </List>
    </>
  )
}