import englishMessages from 'ra-language-english'
import {TranslationMessages} from 'react-admin'
import en from './en.json'
import es from './es.json'

/**
 * Supported translations messages by language code
 */
const LOCALES: Record<string, TranslationMessages> = {
  en: {...englishMessages, ...en},
  es: {...englishMessages, ...es}
}

/**
 * List of supported languages and their name in their own language
 */
const LANG_NAMES: Record<string, string>[] = [
  {text:"English", code: "en"},
  {text:"Español", code: "es"}
]

export {LOCALES, LANG_NAMES}