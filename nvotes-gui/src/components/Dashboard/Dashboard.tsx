// in src/Dashboard.js
import * as React from "react"
import { Card, CardContent, CardHeader } from '@material-ui/core'
import { useTranslate } from 'react-admin'

const Dashboard = () => {
  const translate = useTranslate()
  return (
    <Card>
      <CardHeader title={translate("nvotes.gui.dashboard.welcome")} />
      <CardContent>{translate("nvotes.gui.dashboard.lorem")}</CardContent>
    </Card>
  )
}

export default Dashboard