// The MIT License (MIT)
// 
// Copyright (c) 2016-present, Francois Zaninotto, Marmelab
// Copyright (c) 2018 Fizix
// Copyright (c) 2020-present, Eduardo Robles, nVotes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to 
// deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.

import * as React from 'react'
import {
  Children,
  cloneElement,
  isValidElement,
  useState,
  useRef,
  useCallback,
} from 'react'
import { isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import {
  sanitizeListRestProps,
  Identifier,
  Record,
  DatagridProps as RADatagridProps,
  DatagridLoading,
  DatagridBody,
  PureDatagridBody,
  FieldTitle,
  useDatagridStyles,
  useListContext,
  useTranslate,
  useVersion,
} from 'react-admin'
import {
  Checkbox,
  Table,
  TableCell,
  TableHead,
  TableRow,
  makeStyles,
  ListItem,
  ListItemText,
  ListItemIcon
} from '@material-ui/core'

import MoreVert from '@material-ui/icons/MoreVert'
import classnames from 'classnames'

import PopMenu from '../PopMenu/PopMenu'
import DatagridHeaderCell from './DatagridHeaderCell'
import LocalStorage from './LocalStorage'

const useStyles = makeStyles(
  (theme) => ({
    settingsIcon: {
      position: 'absolute',
      right: -theme.spacing(2.5),
      top: -theme.spacing(1),
      zIndex: 10,
      '& button': {
        padding: theme.spacing(1)
      },
      '& svg': {
        width: theme.spacing(3)
      }
    }
  }),
  { name: 'NvDatagrid' }
)

/**
 * Column definition
 */
interface Column {
  display: boolean
  width: number
}
interface ColsArray {
  [key: string]: Column
}

const arrayToColumns = (columnNames: string[], children: React.ReactNode) => {
  const columns = React.Children
    .map(
      children,
      (field: any) => {
        return {columnName: field?.props?.source, width: field?.props['data-width'] || 100}
      }
    )

  const columnsFiltered: ColsArray = columns
    ?.reduce<ColsArray>(
      (accumColumns: ColsArray, {columnName, width}) => {
        if (columnName && columnNames.indexOf(columnName) !== -1) {
          accumColumns[columnName] = {
            display: true,
            width
          }
        }
        return accumColumns
      },
      {}
    ) as any // needed to asume results it's not undefined or null
  
  return columnsFiltered
}
interface DatagridState {
  modalOpened: boolean,
  columns: ColsArray
}

/**
 * The Datagrid component renders a list of records as a table.
 * It is usually used as a child of the <List> and <ReferenceManyField> components.
 *
 * Props:
 *  - rowStyle
 *
 * @example Display all posts as a datagrid
 * const postRowStyle = (record, index) => ({
 *     backgroundColor: record.nb_views >= 500 ? '#efe' : 'white',
 * });
 * export const PostList = (props) => (
 *     <List {...props}>
 *         <Datagrid rowStyle={postRowStyle}>
 *             <TextField source="id" />
 *             <TextField source="title" />
 *             <TextField source="body" />
 *             <EditButton />
 *         </Datagrid>
 *     </List>
 * );
 *
 * @example Display all the comments of the current post as a datagrid
 * <ReferenceManyField reference="comments" target="post_id">
 *     <Datagrid>
 *         <TextField source="id" />
 *         <TextField source="body" />
 *         <DateField source="created_at" />
 *         <EditButton />
 *     </Datagrid>
 * </ReferenceManyField>
 */
const Datagrid = (props: DatagridProps): JSX.Element|null => {
  const classes = useDatagridStyles(props)
  const {
    defaultColumns = null,
    storage = LocalStorage,
    optimized = false,
    body = optimized ? <PureDatagridBody /> : <DatagridBody />,
    children,
    classes: classesOverride,
    className,
    expand,
    hasBulkActions = false,
    hover,
    isRowSelectable,
    resource,
    rowClick,
    rowStyle,
    size = 'small',
    ...rest
  } = props

  const {
    basePath,
    currentSort,
    data,
    ids,
    loaded,
    onSelect,
    onToggleItem,
    selectedIds,
    setSort,
    total,
  } = useListContext(props)
  const version = useVersion()
  const nvClasses = useStyles()
  const translate = useTranslate()
  const tableRef = useRef<HTMLTableElement>(null)

  const updateSort = useCallback(
    event => {
      event.stopPropagation()
      const newField = event.currentTarget.dataset.field
      const newOrder =
        currentSort.field === newField
          ? currentSort.order === 'ASC'
            ? 'DESC'
            : 'ASC'
          : event.currentTarget.dataset.order

      setSort(newField, newOrder)
    },
    [currentSort.field, currentSort.order, setSort]
  )

  const handleSelectAll = useCallback(
    event => {
      if (event.target.checked) {
        const all = ids.concat(
          selectedIds.filter(id => !ids.includes(id))
        )
        onSelect(
          isRowSelectable
            ? all.filter(id => isRowSelectable(data[id]))
            : all
        )
      } else {
        onSelect([])
      }
    },
    [data, ids, onSelect, isRowSelectable, selectedIds]
  )

  const all = isRowSelectable
    ? ids.filter(id => isRowSelectable(data[id]))
    : ids
  
  const getColumnNames = useCallback(
    () => {
      return React.Children
        .map(
          children, 
          (field: any) => field.props.source
        )
    },
    [children]
  )

  const getInitialColumns = useCallback(
    () => {
      if (resource && !isEmpty(resource)) {
        const previousColumns: ColsArray = storage.get(resource)
  
        // if we have a previously stored value, let's return it
        if (!isEmpty(previousColumns)) {
          return previousColumns
        }
      }

      // if defaultColumns are set let's return them
      if (defaultColumns && !isEmpty(defaultColumns)) {
        return arrayToColumns(defaultColumns, children)
      }

      // otherwise we fallback on the default behaviour : display all columns
      return arrayToColumns(getColumnNames() || [], children)
    },
    [ defaultColumns, resource, storage, getColumnNames, children ]
  )

  const initialColumns = getInitialColumns()
  const filteredChildren = Children
    .map(
      children, 
      field => field
    )
    ?.filter(field => 
      isValidElement(field) && initialColumns[field.props.source].display
    )
    // set dataIsLast
    ?.map((field: any, index, array) => cloneElement(
      field,
      {
        "data-is-last": isValidElement(field) && (index === array.length - 1)
      }
    )) || []

  const [state, setState] = useState<DatagridState>({
    modalOpened: false,
    columns: initialColumns
  })
  const colDivisor: number = filteredChildren.reduce<number>(
    (accum, field) => accum + state.columns[(field as any).props.source].width,
    0
  )

  // updates the storage with the internal state value
  const updateStorage = useCallback(
    (newColumns) => {
      if (resource) {
        storage.set(resource, newColumns)
      }
    },
    [storage, resource]
  )

  const toggleColumn = useCallback(
    (columnName: string) => {
      const previousColumns = state.columns
      const newColumns: ColsArray = {
        ...previousColumns,
        [columnName]: {
          ...previousColumns[columnName],
          display: !previousColumns[columnName].display
        }
      }
      const newState: DatagridState = {
        ...state,
        columns: newColumns
      }
      
      updateStorage(newColumns)
      setState(newState)
    },
    [state, setState, updateStorage]
  )

  const setColumnWidth = useCallback(
    (columnName: string, newPixelWidth: number) => {
      const colsArray: Column[] = filteredChildren
        .map(
          (field: any, index) => state.columns[(field as any).props.source]
        )
      const colNames : string[] = filteredChildren
        .map(
          (field: any) => field.props.source
        )
      const previousColumns = state.columns

      if (!filteredChildren || !tableRef.current) {
        return
      }

      // obtain current column and next column width
      const currentIndex = colNames.indexOf(columnName)
      if (currentIndex === -1 || currentIndex === colsArray.length - 1) {
        return
      }
      const newAbsoluteWidth = (newPixelWidth * colDivisor)/tableRef.current.offsetWidth
      let newColumns: ColsArray = {
        ...previousColumns,
        [columnName]: {
          ...previousColumns[columnName],
          width: newAbsoluteWidth
        }
      }
      const nextColumnName = colNames[currentIndex+1]
      const diffWidth = newAbsoluteWidth - colsArray[currentIndex].width
      if (Math.abs(diffWidth) < 1) {
        return
      }
      const nextColWidth = Math.min(
        Math.max(
          previousColumns[nextColumnName].width - diffWidth,
          0
        ),
        colDivisor
      )
      newColumns[nextColumnName] = {
        ...previousColumns[nextColumnName],
        width: nextColWidth
      }

      const newState: DatagridState = {
        ...state,
        columns: newColumns
      }
      
      updateStorage(newColumns)
      setState(newState)
    },
    [state, setState, updateStorage, filteredChildren, colDivisor] 
  )

  /**
   * if loaded is false, the list displays for the first time, and the dataProvider hasn't answered yet
   * if loaded is true, the data for the list has at least been returned once by the dataProvider
   * if loaded is undefined, the Datagrid parent doesn't track loading state (e.g. ReferenceArrayField)
   */
  if (loaded === false) {
    return (
      <DatagridLoading
        classes={classes}
        className={className}
        expand={expand}
        hasBulkActions={hasBulkActions}
        nbChildren={React.Children.count(children)}
        size={size}
      />
    )
  }

  /**
   * Once loaded, the data for the list may be empty. Instead of
   * displaying the table header with zero data rows,
   * the datagrid displays nothing in this case.
   */
  if (loaded && (ids.length === 0 || total === 0)) {
    return null
  }

  // pass settings to last children
  const settings =
    <PopMenu
      className={nvClasses.settingsIcon} 
      icon={<MoreVert />}
      label={translate('nvotes.gui.datagrid.settings')}
      placement='bottom-end'
    >
      <div>
        {React.Children.map(
          children,
          (field, index) => isValidElement(field) && (<ListItem
            key={`${field.props.source}`}
            role="listitem" 
            button
            onMouseDown={() => toggleColumn(field.props.source)}
            >
            <ListItemIcon>
              <Checkbox
                checked={state.columns[field.props.source].display}
                tabIndex={index}
                disableRipple
                />
            </ListItemIcon>
            <ListItemText 
              primary={<FieldTitle
                label={field?.props.label}
                source={field?.props.source}
                resource={resource}
              />} 
            />
          </ListItem>)
        )}
      </div>
    </PopMenu>

  /**
   * After the initial load, if the data for the list isn't empty,
   * and even if the data is refreshing (e.g. after a filter change),
   * the datagrid displays the current data.
   */
  return (
    <Table
        ref={tableRef}
        className={classnames(classes.table, className)}
        size={size}
        {...sanitizeListRestProps(rest)}
    >
      <TableHead className={classes.thead}>
          <TableRow
              className={classnames(classes.row, classes.headerRow)}
          >
            {expand && (
              <TableCell
                padding="none"
                className={classnames(
                  classes.headerCell,
                  classes.expandHeader
                )}
              />
            )}
            {hasBulkActions && (
              <TableCell
                padding="checkbox"
                className={classes.headerCell}
              >
                <Checkbox
                  className="select-all"
                  color="primary"
                  checked={
                    selectedIds.length > 0 &&
                    all.length > 0 &&
                    all.every(id => selectedIds.includes(id))
                  }
                  onChange={handleSelectAll}
                />
              </TableCell>
            )}
            {filteredChildren?.map((
              field: any, index) =>
              (isValidElement(field) &&
                <DatagridHeaderCell
                  className={classes.headerCell}
                  currentSort={currentSort}
                  extraBody={
                    index === filteredChildren.length - 1
                      ? settings
                      : undefined
                  }
                  field={field}
                  isSorting={
                    currentSort.field ===
                    (
                      (field.props as any).sortBy ||
                      (field.props as any).source
                    )
                  }
                  key={(field.props as any).source || index}
                  resource={resource as string}
                  updateSort={updateSort}
                  percentWidth={state.columns[(field.props as any).source].width / colDivisor}
                  setWidth={
                    (width: number) => 
                      setColumnWidth((field.props as any).source, width)
                  }
                />)
            )}
          </TableRow>
      </TableHead>
      {cloneElement(
        body,
        {
          basePath,
          className: classes.tbody,
          classes,
          expand,
          rowClick,
          data,
          hasBulkActions,
          hover,
          ids,
          onToggleItem,
          resource,
          rowStyle,
          selectedIds,
          isRowSelectable,
          version,
        },
        filteredChildren
      )}
    </Table>
  )
}

Datagrid.propTypes = {
  basePath: PropTypes.string,
  body: PropTypes.element,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object,
  className: PropTypes.string,
  currentSort: PropTypes.shape({
    field: PropTypes.string,
    order: PropTypes.string,
  }),
  data: PropTypes.object,
  defaultColumns: PropTypes.arrayOf(PropTypes.string.isRequired),
  // @ts-ignore
  expand: PropTypes.oneOfType([PropTypes.element, PropTypes.elementType]),
  hasBulkActions: PropTypes.bool,
  hover: PropTypes.bool,
  ids: PropTypes.arrayOf(PropTypes.any),
  loading: PropTypes.bool,
  onSelect: PropTypes.func,
  onToggleItem: PropTypes.func,
  resource: PropTypes.string,
  rowClick: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  rowStyle: PropTypes.func,
  selectedIds: PropTypes.arrayOf(PropTypes.any),
  setSort: PropTypes.func,
  storage: PropTypes.shape({
    get: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
  }),
  total: PropTypes.number,
  version: PropTypes.number,
  isRowSelectable: PropTypes.func,
}

type RowClickFunction = (
  id: Identifier,
  basePath: string,
  record: Record
) => string

export interface DatagridProps extends RADatagridProps {
  defaultColumns?: Array<string>
  storage?: {
    get: (key: string) => any,
    set: (key: string, val: any) => void
  }
}

export default Datagrid
