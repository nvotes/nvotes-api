import React, { useContext, useEffect } from "react"
import { 
  List, 
  TextField, 
  DateField, 
  TextInput,
  SimpleForm,
  Create,
  Edit,
  Filter,
} from 'react-admin'
import { v4 as uuid_v4 } from 'uuid'
import { TagsInput } from '../../components/TagsInput/TagsInput'
import { UuidInput } from '../../components/UuidInput/UuidInput'
import { AppContext } from "../../providers/AppContext"
import AppBreadcrumbs from "../../components/AppBreadcrumbs/AppBreadcrumbs"
import Datagrid from '../../components/Datagrid/Datagrid'

const UserFilter = (props: any) => (
  <Filter {...props}>
    <TextInput 
      label="Search" 
      source="id@_like,"
      alwaysOn />
  </Filter>
)

const initialValues = () => {
  return {
    id: uuid_v4(),
    labels: [
      {
        "key": "nv:name",
        "value": "some value"
      }
    ],
    annotations: []
  }
}

const UserForm = (props: any) => (
  <SimpleForm {...props} initialValues={initialValues}>
    <UuidInput 
      source="id"
      fullWidth
    />
    <TagsInput
      source="labels"
      fullWidth
    />
    <TagsInput
      source="annotations"
      fullWidth
    />
  </SimpleForm>
)

export const UserCreate = (props: any) => {
  const appContext = useContext(AppContext)
  useEffect(() => {
    appContext.setBreadcrumbs?.('users.create')
  })

  return (
    <>
      <AppBreadcrumbs />
      <Create {...props}>
        <UserForm />
      </Create>
    </>
  )
}

export const UserEdit = (props: any) => {
  const { id } = props
  const appContext = useContext(AppContext)
  useEffect(() => {
    appContext.setBreadcrumbs?.('users.edit', {id: id})
  })

  return (
    <>
      <AppBreadcrumbs />
      <Edit {...props}>
        <UserForm />
      </Edit>
    </>
  )
}

export const UserList = (props: any) => {
  const appContext = useContext(AppContext)
  useEffect(() => {
    appContext.setBreadcrumbs?.('users.list')
  })

  return (
    <>
      <AppBreadcrumbs />
      <List filters={<UserFilter />} {...props}>
        <Datagrid rowClick="edit">
          <TextField source="id" />
          <DateField source="created_at" />
          <DateField source="updated_at" />
          <TextField source="labels" />
          <TextField source="annotations" />
        </Datagrid>
      </List>
    </>
  )
}