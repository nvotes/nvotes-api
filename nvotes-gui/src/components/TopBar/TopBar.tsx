import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import useScrollTrigger from '@material-ui/core/useScrollTrigger'

interface Props {
  children: React.ReactElement
}

function ElevationScroll(props: Props) 
{
  const trigger: boolean = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0
  })

  return React.cloneElement(props.children, {
    elevation: trigger ? 20 : 0,
  })
}

/**
 * Top navigation bar that shows a shadow when user scroll, giving the 
 * appearance of elevating the navigation bar and thus making it visible that
 * the user has made scroll.
 */
class TopBar extends React.Component<Props> {
  render() {
    return (
      <ElevationScroll>
        <AppBar>
          {this.props.children}
        </AppBar>
      </ElevationScroll>
    )
  }
}

export default TopBar