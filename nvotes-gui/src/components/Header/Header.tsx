import React, { useState, useContext } from 'react'
import TopBar from '../TopBar/TopBar'
import Toolbar from '@material-ui/core/Toolbar'
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton'
import Brightness4Icon from '@material-ui/icons/Brightness4'
import Brightness7Icon from '@material-ui/icons/Brightness7'
import Typography from '@material-ui/core/Typography'
import { useIntl, FormattedMessage } from "react-intl"
import styled from 'styled-components'

import LanguageChooser from '../LanguageChooser/LanguageChooser'
import { AppContext } from '../../providers/AppContext'

const RightSide = styled.div`
  margin-left: auto;
`

const Header = () => {
  const intl = useIntl()
  const [isDarkTheme, setDarkTheme] = useState(false)
  const appContext = useContext(AppContext)

  const handleTogglePaletteType = () => {
    const toggledVal = !isDarkTheme
    appContext.setThemeOptions?.({
      ...appContext.themeOptions,
      palette: {
        type: toggledVal ? 'dark' : 'light'
      }
    })
    setDarkTheme(toggledVal)
  }

  return (
    <header className="App-header">
      <TopBar>
        <Toolbar>
            <Typography variant="h6">
              <FormattedMessage id="app_name" />
            </Typography>

            <RightSide>
            </RightSide>
            <LanguageChooser />

            <Tooltip
              title={intl.formatMessage({id:'toggleTheme'})}
              enterDelay={300}
            >
              <IconButton
                color="inherit"
                onClick={handleTogglePaletteType}
                aria-label={intl.formatMessage({id:'toggleTheme'})}
              >
                {isDarkTheme ? <Brightness4Icon /> : <Brightness7Icon />}
              </IconButton>
            </Tooltip>
          </Toolbar>
      </TopBar>
    </header>
  )
}

export default Header
