import * as React from 'react'
import LocationCityIcon from '@material-ui/icons/LocationCity'
import LabelIcon from '@material-ui/icons/Label'
import HelpIcon from '@material-ui/icons/Help'
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer'
import ListAltIcon from '@material-ui/icons/ListAlt'
import GroupIcon from '@material-ui/icons/Group'
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck'
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople'
import SubscriptionsIcon from '@material-ui/icons/Subscriptions'
import AlarmOnIcon from '@material-ui/icons/AlarmOn'
import PermDataSettingIcon from '@material-ui/icons/PermDataSetting'
import EqualizerIcon from '@material-ui/icons/Equalizer'
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered'
import MailOutlineIcon from '@material-ui/icons/MailOutline'
import WebAssetIcon from '@material-ui/icons/WebAsset'
import { EditGuesser, ListGuesser, Resource } from 'react-admin'
import { UserList, UserCreate, UserEdit } from './User'
import { OrganizationList } from './Organization'

export const userResources = [
  /* Election resources */
  <Resource 
    key='election-parent'
    name='election-parent' 
    options={{
      label: 'nvotes.gui.menu.election',
      isMenuParent: true
    }}
    icon={ListAltIcon}
  />,
  <Resource
    key='Organization'
    name='Organization'
    options={{
      label: 'nvotes.gui.menu.organizations',
      menuParent: 'election-parent'
    }}
    icon={LocationCityIcon}
    list={OrganizationList}
    edit={EditGuesser}
  />,
  <Resource
    key='ContestOption'
    name='ContestOption'
    options={{
      label: 'nvotes.gui.menu.contestOptions',
      menuParent: 'election-parent'
    }}
    icon={LabelIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='Contest'
    name='Contest'
    options={{
      label: 'nvotes.gui.menu.contests', 
      menuParent: 'election-parent'
    }}
    icon={HelpIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='CodecContestGroup'
    name='CodecContestGroup'
    options={{
      label: 'nvotes.gui.menu.codecContestGroups',
      menuParent: 'election-parent'
    }}
    icon={QuestionAnswerIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='Election'
    name='Election'
    options={{
      label: 'nvotes.gui.menu.elections',
      menuParent: 'election-parent'
    }}
    icon={ListAltIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='ElectoralProcess'
    name='ElectoralProcess'
    options={{
      label: 'nvotes.gui.menu.electoralProcesses',
      menuParent: 'election-parent'
    }}
    icon={SubscriptionsIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,

  /* Vote resources */
  <Resource 
    key='vote-parent'
    name='vote-parent'
    options={{
      label: 'nvotes.gui.menu.voting',
      isMenuParent: true
    }} 
    icon={MailOutlineIcon}
  />,
  <Resource
    key='Ballot'
    name='Ballot'
    options={{
      label: 'nvotes.gui.menu.ballots',
      menuParent: 'vote-parent'
    }}
    icon={PlaylistAddCheckIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='VoteDelegation'
    name='VoteDelegation'
    options={{
      label: 'nvotes.gui.menu.voteDelegations',
      menuParent: 'vote-parent'
    }}
    icon={EmojiPeopleIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='VotingPeriod'
    name='VotingPeriod'
    options={{
      label: 'nvotes.gui.menu.votingPeriods',
      menuParent: 'vote-parent'
    }}
    icon={AlarmOnIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,


  /* Identity resources */
  <Resource 
    key='identity-parent'
    name='identity-parent' 
    options={{
      label: 'nvotes.gui.menu.identity',
      isMenuParent: true
    }} 
    icon={GroupIcon}
  />,
  <Resource
    key='User'
    name='User'
    options={{
      label: 'nvotes.gui.menu.users',
      menuParent: 'identity-parent'
    }}
    icon={GroupIcon}
    list={UserList}
    create={UserCreate}
    edit={UserEdit}
  />,

  /* Tally resources */
  <Resource 
    key='tally-parent'
    name='tally-parent' 
    options={{
      label: 'nvotes.gui.menu.tally',
      isMenuParent: true
    }}
    icon={EqualizerIcon}
  />,
  <Resource
    key='ContestTallyConfiguration'
    name='ContestTallyConfiguration'
    options={{
      label: 'nvotes.gui.menu.contestTallyConfigs',
      menuParent: 'tally-parent'
    }}
    icon={PermDataSettingIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='ContestTallyResults'
    name='ContestTallyResults'
    options={{
      label: 'nvotes.gui.menu.contestTallyResults',
      menuParent: 'tally-parent'
    }}
    icon={EqualizerIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='TallySheet'
    name='TallySheet'
    options={{
      label: 'nvotes.gui.menu.tallySheets',
      menuParent: 'tally-parent'
    }}
    icon={FormatListNumberedIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />,
  <Resource
    key='ResultsSite'
    name='ResultsSite'
    options={{
      label: 'nvotes.gui.menu.resultSites',
      menuParent: 'tally-parent'
    }}
    icon={WebAssetIcon}
    list={ListGuesser}
    edit={EditGuesser}
  />
]