// The MIT License (MIT)
// 
// Copyright (c) 2016-present, Francois Zaninotto, Marmelab
// Copyright (c) 2020-present, Eduardo Robles, nVotes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to 
// deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.

import * as React from 'react'
import { memo, useRef } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { TableCell, TableSortLabel, Tooltip } from '@material-ui/core'
import { TableCellProps } from '@material-ui/core/TableCell'
import { makeStyles } from '@material-ui/core/styles'
import {
  FieldTitle,
  useTranslate,
  SortPayload,
  useResourceContext,
  ClassesOverride
} from 'react-admin'
import { Resizable } from "re-resizable"

const useStyles = makeStyles(
  (theme) => ({
    icon: {
      display: 'none',
    },
    active: {
      '& $icon': {
        display: 'inline',
      },
    },
    resizable: {
      borderRight: "5px solid " + theme.palette.action.hover,
      '&:hover': {
        borderRightColor: theme.palette.action.selected
      },
      '&.last': {
        borderRight: 'none'
      }
    },
    sortLabel: {
      width: '100%'
    },
    fieldTitle: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap'
    },
    cell: {},
  }),
  { name: 'RaDatagridHeaderCell' }
)

export const DatagridHeaderCell = (
    props: DatagridHeaderCellProps
): JSX.Element => {
  const {
    className,
    classes: classesOverride,
    extraBody,
    field,
    currentSort,
    updateSort,
    isSorting,
    percentWidth,
    setWidth,
    ...rest
  } = props;
  const resource = useResourceContext(props)
  const classes = useStyles(props)
  const translate = useTranslate()
  const cellRef = useRef<HTMLElement>(null)
  const minWidth = 50;
  const onResizeInner = (
    _event: MouseEvent | TouchEvent,
    _direction: any,
    elementRef: HTMLElement,
    delta: any
  ) => {
    // @ts-ignore: Object is possibly 'null'.
    const newWidth = Math.max(elementRef.offsetWidth, minWidth)
    if (setWidth) {
      setWidth(newWidth)
    }
  }

  // find the last field
  const isLast = field?.props['data-is-last'] === true

  const fieldTitle = <span className={classes.fieldTitle}>
    <FieldTitle
      label={field?.props.label}
      source={field?.props.source}
      resource={resource}
    />
  </span>

  return (
    <TableCell
      className={classnames(className, classes.cell, field?.props.headerClassName)}
      align={field?.props.textAlign}
      ref={cellRef}
      variant="head"
      style={{width: percentWidth*100 + '%'}}
      {...rest}
    >
      <Resizable
        className={classnames(classes.resizable, {last: isLast})}
        minWidth={minWidth}
        size={{width: "100%", height: '100%'}}
        onResize={onResizeInner}
        grid={[10, 10]}

        enable={{
          top: false,
          right: !isLast,
          bottom: false,
          left: false,
          topRight: false,
          bottomRight: false,
          bottomLeft: false,
          topLeft: false
        }}
      >
        {(
          field?.props.sortable !== false && 
          (field?.props.sortBy || field?.props.source)
        )
          ? (
            <Tooltip
              title={translate('ra.action.sort')}
              placement={
                field?.props.textAlign === 'right'
                  ? 'bottom-end'
                  : 'bottom-start'
              }
              enterDelay={300}
            >
              <>
                <TableSortLabel
                  active={
                    currentSort.field ===
                    (field.props.sortBy || field.props.source)
                  }
                  direction={currentSort.order === 'ASC' ? 'asc' : 'desc'}
                  data-sort={field.props.sortBy || field.props.source} // @deprecated. Use data-field instead.
                  data-field={field.props.sortBy || field.props.source}
                  data-order={field.props.sortByOrder || 'ASC'}
                  onClick={updateSort}
                  classes={{icon: classes.icon, active: classes.active, root: classes.sortLabel}}
                >
                  {fieldTitle}
                </TableSortLabel>
                {extraBody}
              </>
            </Tooltip>
          )
          : fieldTitle
        }
      </Resizable>
    </TableCell>
  )
}

export interface DatagridHeaderCellProps 
extends Omit<TableCellProps, 'classes'> 
{
  className?: string
  classes?: ClassesOverride<typeof useStyles>
  currentSort: SortPayload
  extraBody?: JSX.Element
  field?: JSX.Element
  isSorting?: boolean
  minWidth?: number
  setWidth?: (width: number) => void
  resource: string
  updateSort: (event: any) => void
  percentWidth: number
}

DatagridHeaderCell.propTypes = {
  className: PropTypes.string,
  classes: PropTypes.object,
  currentSort: PropTypes.shape({
    sort: PropTypes.string,
    order: PropTypes.string,
  }).isRequired,
  extraBody: PropTypes.element,
  field: PropTypes.element,
  isSorting: PropTypes.bool,
  minWidth: PropTypes.number,
  setWidth: PropTypes.func.isRequired,
  resource: PropTypes.string,
  updateSort: PropTypes.func.isRequired,
  percentWidth: PropTypes.number.isRequired
}

export default memo(
  DatagridHeaderCell,
  (props, nextProps) =>
    props.updateSort === nextProps.updateSort &&
    props.currentSort.field === nextProps.currentSort.field &&
    props.currentSort.order === nextProps.currentSort.order &&
    props.isSorting === nextProps.isSorting &&
    props.resource === nextProps.resource &&
    props.extraBody === nextProps.extraBody &&
    props.percentWidth === nextProps.percentWidth &&
    props.setWidth === nextProps.setWidth
)
