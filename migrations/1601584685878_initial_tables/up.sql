CREATE FUNCTION public.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;
CREATE TABLE public."Ballot" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    ballot jsonb NOT NULL,
    ballot_type text NOT NULL,
    voter_id uuid NOT NULL,
    org_id uuid NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    annotations jsonb DEFAULT jsonb_build_object() NOT NULL,
    election_id uuid NOT NULL
);
CREATE TABLE public."CodecContestGroup" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    labels jsonb NOT NULL,
    annotations jsonb NOT NULL,
    codec jsonb NOT NULL,
    org_id uuid NOT NULL
);
CREATE TABLE public."CodecContestGroup_Contest" (
    codec_contest_group_id uuid NOT NULL,
    contest_id uuid NOT NULL,
    sort_id integer NOT NULL,
    org_id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now()
);
CREATE TABLE public."Contest" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    org_id uuid NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    annotations jsonb DEFAULT jsonb_build_object() NOT NULL,
    presentation_policy jsonb NOT NULL
);
CREATE TABLE public."ContestOption" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    title text NOT NULL,
    description text,
    labels jsonb,
    annotations jsonb,
    org_id uuid NOT NULL
);
CREATE TABLE public."ContestTallyConfiguration" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    configuration jsonb DEFAULT jsonb_build_object() NOT NULL,
    org_id uuid NOT NULL
);
CREATE TABLE public."ContestTallyConfiguration_Election" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    org_id uuid NOT NULL,
    election_id uuid NOT NULL,
    contest_id uuid NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    annotations jsonb DEFAULT jsonb_build_object() NOT NULL
);
CREATE TABLE public."ContestTallyResults" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    results jsonb NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    annotations jsonb DEFAULT jsonb_build_object() NOT NULL,
    org_id uuid NOT NULL,
    contest_tally_configuration_id uuid NOT NULL
);
CREATE TABLE public."Contest_ContestOption" (
    contest_id uuid NOT NULL,
    contest_option_id uuid NOT NULL,
    sort_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    org_id uuid NOT NULL
);
CREATE TABLE public."Election" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    annotations jsonb DEFAULT jsonb_build_object() NOT NULL,
    presentation jsonb DEFAULT jsonb_build_object() NOT NULL,
    title text,
    description text,
    org_id uuid NOT NULL,
    electoral_process_id uuid,
    configuration jsonb DEFAULT jsonb_build_object() NOT NULL,
    voting_period_id uuid NOT NULL
);
CREATE TABLE public."Election_CodecContestGroup" (
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    election_id uuid NOT NULL,
    codec_contest_group_id uuid NOT NULL,
    sort_id integer NOT NULL,
    org_id uuid NOT NULL
);
CREATE TABLE public."ElectoralProcess" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    annotations jsonb DEFAULT jsonb_build_object() NOT NULL,
    configuration jsonb NOT NULL,
    org_id uuid NOT NULL
);
CREATE TABLE public."Organization" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    name text NOT NULL
);
CREATE TABLE public."ResultsSite" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    org_id uuid NOT NULL,
    configuration jsonb DEFAULT jsonb_build_object() NOT NULL,
    labels jsonb NOT NULL,
    annotations jsonb NOT NULL
);
CREATE TABLE public."TallySheet" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    org_id uuid NOT NULL,
    data jsonb DEFAULT jsonb_build_object() NOT NULL,
    election_id uuid NOT NULL
);
CREATE TABLE public."User" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    org_id uuid NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    annotations jsonb DEFAULT jsonb_build_object() NOT NULL
);
CREATE TABLE public."VoteDelegation" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    "from" uuid NOT NULL,
    "to" uuid,
    election_id uuid NOT NULL,
    configuration jsonb DEFAULT jsonb_build_object() NOT NULL,
    labels jsonb DEFAULT jsonb_build_object() NOT NULL,
    org_id uuid NOT NULL
);
CREATE TABLE public."VotingPeriod" (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    labels jsonb NOT NULL,
    annotations jsonb NOT NULL,
    configuration jsonb NOT NULL,
    org_id uuid NOT NULL
);
ALTER TABLE ONLY public."Ballot"
    ADD CONSTRAINT "Ballot_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."CodecContestGroup_Contest"
    ADD CONSTRAINT "CodecContestGroup_Contest_pkey" PRIMARY KEY (codec_contest_group_id, contest_id);
ALTER TABLE ONLY public."CodecContestGroup"
    ADD CONSTRAINT "CodecContestGroup_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."ContestOption"
    ADD CONSTRAINT "ContestOption_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."ContestTallyConfiguration_Election"
    ADD CONSTRAINT "ContestTallyConfiguration_Election_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."ContestTallyConfiguration"
    ADD CONSTRAINT "ContestTallyConfiguration_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."ContestTallyResults"
    ADD CONSTRAINT "ContestTallyResults_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."Contest_ContestOption"
    ADD CONSTRAINT "Contest_ContestOption_pkey" PRIMARY KEY (contest_id, contest_option_id);
ALTER TABLE ONLY public."Contest"
    ADD CONSTRAINT "Contest_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."Election_CodecContestGroup"
    ADD CONSTRAINT "Election_CodecContestGroup_pkey" PRIMARY KEY (election_id, codec_contest_group_id);
ALTER TABLE ONLY public."Election"
    ADD CONSTRAINT "Election_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."ElectoralProcess"
    ADD CONSTRAINT "ElectoralProcess_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."Organization"
    ADD CONSTRAINT "Organization_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."ResultsSite"
    ADD CONSTRAINT "ResultsSite_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."TallySheet"
    ADD CONSTRAINT "TallySheet_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."VoteDelegation"
    ADD CONSTRAINT "VoteDelegation_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."VotingPeriod"
    ADD CONSTRAINT "VotingStatus_pkey" PRIMARY KEY (id);
CREATE TRIGGER "set_public_Ballot_updated_at" BEFORE UPDATE ON public."Ballot" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_Ballot_updated_at" ON public."Ballot" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_CodecContestGroup_Contest_updated_at" BEFORE UPDATE ON public."CodecContestGroup_Contest" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_CodecContestGroup_Contest_updated_at" ON public."CodecContestGroup_Contest" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_CodecContestGroup_updated_at" BEFORE UPDATE ON public."CodecContestGroup" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_CodecContestGroup_updated_at" ON public."CodecContestGroup" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_ContestOption_updated_at" BEFORE UPDATE ON public."ContestOption" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_ContestOption_updated_at" ON public."ContestOption" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_ContestTallyConfiguration_Election_updated_at" BEFORE UPDATE ON public."ContestTallyConfiguration_Election" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_ContestTallyConfiguration_Election_updated_at" ON public."ContestTallyConfiguration_Election" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_ContestTallyConfiguration_updated_at" BEFORE UPDATE ON public."ContestTallyConfiguration" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_ContestTallyConfiguration_updated_at" ON public."ContestTallyConfiguration" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_ContestTallyResults_updated_at" BEFORE UPDATE ON public."ContestTallyResults" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_ContestTallyResults_updated_at" ON public."ContestTallyResults" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_Contest_ContestOption_updated_at" BEFORE UPDATE ON public."Contest_ContestOption" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_Contest_ContestOption_updated_at" ON public."Contest_ContestOption" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_Contest_updated_at" BEFORE UPDATE ON public."Contest" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_Contest_updated_at" ON public."Contest" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_Election_CodecContestGroup_updated_at" BEFORE UPDATE ON public."Election_CodecContestGroup" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_Election_CodecContestGroup_updated_at" ON public."Election_CodecContestGroup" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_Election_updated_at" BEFORE UPDATE ON public."Election" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_Election_updated_at" ON public."Election" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_ElectoralProcess_updated_at" BEFORE UPDATE ON public."ElectoralProcess" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_ElectoralProcess_updated_at" ON public."ElectoralProcess" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_Organization_updated_at" BEFORE UPDATE ON public."Organization" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_Organization_updated_at" ON public."Organization" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_ResultsSite_updated_at" BEFORE UPDATE ON public."ResultsSite" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_ResultsSite_updated_at" ON public."ResultsSite" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_TallySheet_updated_at" BEFORE UPDATE ON public."TallySheet" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_TallySheet_updated_at" ON public."TallySheet" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_User_updated_at" BEFORE UPDATE ON public."User" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_User_updated_at" ON public."User" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_VoteDelegation_updated_at" BEFORE UPDATE ON public."VoteDelegation" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_VoteDelegation_updated_at" ON public."VoteDelegation" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER "set_public_VotingStatus_updated_at" BEFORE UPDATE ON public."VotingPeriod" FOR EACH ROW EXECUTE PROCEDURE public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER "set_public_VotingStatus_updated_at" ON public."VotingPeriod" IS 'trigger to set value of column "updated_at" to current timestamp on row update';
ALTER TABLE ONLY public."Ballot"
    ADD CONSTRAINT "Ballot_election_id_fkey" FOREIGN KEY (election_id) REFERENCES public."Election"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Ballot"
    ADD CONSTRAINT "Ballot_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."CodecContestGroup_Contest"
    ADD CONSTRAINT "CodecContestGroup_Contest_codec_contest_group_id_fkey" FOREIGN KEY (codec_contest_group_id) REFERENCES public."CodecContestGroup"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."CodecContestGroup_Contest"
    ADD CONSTRAINT "CodecContestGroup_Contest_contest_id_fkey" FOREIGN KEY (contest_id) REFERENCES public."Contest"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."CodecContestGroup_Contest"
    ADD CONSTRAINT "CodecContestGroup_Contest_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."CodecContestGroup"
    ADD CONSTRAINT "CodecContestGroup_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ContestOption"
    ADD CONSTRAINT "ContestOption_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ContestTallyConfiguration_Election"
    ADD CONSTRAINT "ContestTallyConfiguration_Election_contest_id_fkey" FOREIGN KEY (contest_id) REFERENCES public."Contest"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ContestTallyConfiguration_Election"
    ADD CONSTRAINT "ContestTallyConfiguration_Election_election_id_fkey" FOREIGN KEY (election_id) REFERENCES public."Election"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ContestTallyConfiguration_Election"
    ADD CONSTRAINT "ContestTallyConfiguration_Election_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ContestTallyConfiguration"
    ADD CONSTRAINT "ContestTallyConfiguration_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ContestTallyResults"
    ADD CONSTRAINT "ContestTallyResults_contest_tally_configuration_id_fkey" FOREIGN KEY (contest_tally_configuration_id) REFERENCES public."ContestTallyConfiguration"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ContestTallyResults"
    ADD CONSTRAINT "ContestTallyResults_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Contest_ContestOption"
    ADD CONSTRAINT "Contest_ContestOption_contest_id_fkey" FOREIGN KEY (contest_id) REFERENCES public."Contest"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Contest_ContestOption"
    ADD CONSTRAINT "Contest_ContestOption_contest_option_id_fkey" FOREIGN KEY (contest_option_id) REFERENCES public."ContestOption"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Contest_ContestOption"
    ADD CONSTRAINT "Contest_ContestOption_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Contest"
    ADD CONSTRAINT "Contest_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Election_CodecContestGroup"
    ADD CONSTRAINT "Election_CodecContestGroup_codec_contest_group_id_fkey" FOREIGN KEY (codec_contest_group_id) REFERENCES public."CodecContestGroup"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Election_CodecContestGroup"
    ADD CONSTRAINT "Election_CodecContestGroup_election_id_fkey" FOREIGN KEY (election_id) REFERENCES public."Election"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Election_CodecContestGroup"
    ADD CONSTRAINT "Election_CodecContestGroup_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Election"
    ADD CONSTRAINT "Election_electoral_process_id_fkey" FOREIGN KEY (electoral_process_id) REFERENCES public."ElectoralProcess"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Election"
    ADD CONSTRAINT "Election_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."Election"
    ADD CONSTRAINT "Election_voting_period_id_fkey" FOREIGN KEY (voting_period_id) REFERENCES public."VotingPeriod"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ElectoralProcess"
    ADD CONSTRAINT "ElectoralProcess_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."ResultsSite"
    ADD CONSTRAINT "ResultsSite_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."TallySheet"
    ADD CONSTRAINT "TallySheet_election_id_fkey" FOREIGN KEY (election_id) REFERENCES public."Election"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."TallySheet"
    ADD CONSTRAINT "TallySheet_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."VoteDelegation"
    ADD CONSTRAINT "VoteDelegation_election_id_fkey" FOREIGN KEY (election_id) REFERENCES public."Election"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."VoteDelegation"
    ADD CONSTRAINT "VoteDelegation_from_fkey" FOREIGN KEY ("from") REFERENCES public."User"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."VoteDelegation"
    ADD CONSTRAINT "VoteDelegation_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."VoteDelegation"
    ADD CONSTRAINT "VoteDelegation_to_fkey" FOREIGN KEY ("to") REFERENCES public."User"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public."VotingPeriod"
    ADD CONSTRAINT "VotingStatus_org_id_fkey" FOREIGN KEY (org_id) REFERENCES public."Organization"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
