import * as React from "react"
import {TextInput, ArrayInput, SimpleFormIterator} from 'react-admin'
import { Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  box: {
    width: '100%',
    padding: 0
  },
  form: {
    '& .fade-enter-done, & .fade-enter-active, & .fade-exit-active': {
      border: 'none'
    }
  }
})

export const TagsInput = (props: any) => {
  const classes = useStyles()

  return (
    <ArrayInput {...props}>
      <SimpleFormIterator className={classes.form}>
        <Box p="1em" className={classes.box}>
          <Box display="flex">
            <Box flex={1} mr="0.5em">
              <TextInput source="key" fullWidth />
            </Box>
            <Box flex={1} mr="1em">
              <TextInput source="value" fullWidth />
            </Box>
          </Box>
        </Box>
      </SimpleFormIterator>
    </ArrayInput>
  )
}