import React, { useState, useContext } from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import Tooltip from '@material-ui/core/Tooltip'
import LanguageIcon from '@material-ui/icons/Translate'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Button from '@material-ui/core/Button'
import { useIntl } from "react-intl"

import { LANG_NAMES } from '../../translations'
import { AppContext } from '../../providers/AppContext'

const LanguageChooser = () => {
  const intl = useIntl()
  const appContext = useContext(AppContext)
  const [localeMenu, setLocaleMenu] = useState<Element | null>(null)

  const closeLocaleMenu = () => {
    setLocaleMenu(null)
  }

  const onLocaleChange = (
    e: React.MouseEvent<HTMLElement>
  ) => {
    appContext.setLocale?.(
      (e.currentTarget as HTMLElement).getAttribute('lang') as string
    )
    closeLocaleMenu()
  }

  const showLocaleMenu = (e: React.MouseEvent<HTMLElement>) => {
    setLocaleMenu(e.currentTarget)
  }

  return (
    <div>
      <Tooltip
        title={intl.formatMessage({id: 'changeLanguage'})} 
        enterDelay={300}
      >
        <Button
          color="inherit"
          aria-owns={localeMenu ? 'locale-menu' : undefined}
          aria-haspopup="true"
          onClick={showLocaleMenu}
          aria-label={intl.formatMessage({id: 'changeLanguage'})}
          >
          <LanguageIcon />
          {
            LANG_NAMES
              .filter(
                (language) =>
                  language.code === appContext.locale.split('-')[0]
              )[0]?.text
            }
          <ExpandMoreIcon fontSize="small" />
        </Button>
      </Tooltip>

      <Menu
        id="language-menu"
        anchorEl={localeMenu}
        open={!!localeMenu}
        onClose={closeLocaleMenu}
      >
        {LANG_NAMES.map((language) => (
          <MenuItem
            selected={appContext.locale === language.code}
            onClick={onLocaleChange}
            lang={language.code}
            key={language.code}
          >
            {language.text}
          </MenuItem>
        ))}
      </Menu>
    </div>
  )
}

export default LanguageChooser