import polyglotI18nProvider from 'ra-i18n-polyglot'
import {LOCALES} from '../translations'

const i18nProvider = polyglotI18nProvider(locale => LOCALES[locale])

export default i18nProvider
