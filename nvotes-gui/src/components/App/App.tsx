import ApolloClient from 'apollo-boost'
import buildHasuraProvider from "ra-data-hasura-graphql"
import * as React from "react"
import { Admin } from 'react-admin'

import { userResources } from '../../resources/index'
import AppLayout from '../AppLayout/AppLayout'
import AppMenu from '../AppMenu/AppMenu'
import AppProvider from '../../providers/AppContext'
import i18nProvider from '../../providers/i18n'
import Dashboard from '../Dashboard/Dashboard'

const App = (): JSX.Element => {
  const [hasuraProvider, setHasuraProvider]: any = React.useState()

  React.useEffect(() => {
    if (!hasuraProvider)  {
      const client = new ApolloClient({
        uri: 'http://localhost:8080/v1/graphql',
        headers: {
          'X-Hasura-Role': 'user',
          'X-Hasura-User-Id': 'c73a1c3b-7034-4735-b226-bfb72859887b',
          'X-Hasura-Org-Id': '0f6fb5bf-4228-4b3a-abb6-c9062116b128'

        }
      })

      buildHasuraProvider({client: client})
        .then((newProvider: any) => setHasuraProvider(() => newProvider))
    }
  })

  if (!hasuraProvider) {
    return (<div>Loading...</div>);
  }

  return (
    <AppProvider>
      <Admin
        dataProvider={hasuraProvider}
        layout={AppLayout}
        i18nProvider={i18nProvider}
        menu={AppMenu}
        dashboard={Dashboard}
      >
        {userResources}
      </Admin>
    </AppProvider>
  )
}

export default App
