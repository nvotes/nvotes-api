import React from 'react'
import {
  Children,
  cloneElement,
  isValidElement,
  useEffect, 
  useRef, 
  useState 
} from 'react' 
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Grow from '@material-ui/core/Grow'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import MenuList from '@material-ui/core/MenuList'
import AccountCircle from '@material-ui/icons/AccountCircle'
import { makeStyles, Theme } from '@material-ui/core/styles'
import classNames from 'classnames'
import { useTranslate } from 'react-admin'

const useStyles = makeStyles(
  (theme: Theme) => ({
    root: {
      display: 'flex',
    },
    paper: {
      border: '1px solid ' + theme.palette.primary.main,
      boxShadow: '0 0 20px rgba(80,110,245,.4)',
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1)
    },
    menuItem: {
      color: theme.palette.secondary.main
    },
  }),
  { name: 'PopMenu' }
)

interface Props {
  button?: JSX.Element,
  children: JSX.Element
  className?: string,
  icon?: JSX.Element,
  label: string,
  placement?: "bottom" | "left" | "right" | "top" | "bottom-end" | "bottom-start" | "left-end" | "left-start" | "right-end" | "right-start" | "top-end" | "top-start" | undefined
}

const PopMenu = (props: Props) => {
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const translate = useTranslate()
  const anchorRef = useRef<HTMLButtonElement>(null)
  const {
    children = <div />,
    label = 'ra.auth.user_menu',
    icon = <AccountCircle />,
    className = undefined,
    button = undefined,
    placement = undefined
  } = props

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }

  const handleClose = (event: React.MouseEvent<EventTarget>) => {
    if (
      anchorRef.current && 
      anchorRef.current.contains(event.target as HTMLElement)
    ) {
      return
    }

    setOpen(false)
  }

  const handleListKeyDown = (event: React.KeyboardEvent) => {
    if (event.key === 'Tab') {
      event.preventDefault()
      setOpen(false)
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = useRef(open)
  useEffect(
    () => {
      if (prevOpen.current === true && open === false) {
        anchorRef.current!.focus()
      }

      prevOpen.current = open
    }, 
    [open]
  )

  return (
    <div className={classNames(classes.root, className)}>
      <Tooltip 
        title={label && translate(label, { _: label })}
        enterDelay={500}
      >
        {isValidElement(button) ?
          cloneElement<any>(button, {
            ref: anchorRef,
            "aria-label": label && translate(label, { _: label }),
            "aria-controls": open ? 'pop-menu-list-grow' : undefined,
            "aria-haspopup": "true",
            onClick: handleToggle
          })
          : <IconButton
              ref={anchorRef}
              aria-label={label && translate(label, { _: label })}
              aria-controls={open ? 'pop-menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={handleToggle}
            >
              {icon}
            </IconButton>
        }
      </Tooltip>
      <Popper 
        open={open} 
        anchorEl={anchorRef.current} 
        role={undefined} 
        transition
        disablePortal={true}
        placement={placement}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper classes={{root: classes.paper}}>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList 
                  autoFocusItem={open} 
                  id="menu-list-grow" 
                  onKeyDown={handleListKeyDown}
                >
                {Children.map(
                  children, 
                  menuItem =>
                    isValidElement(menuItem)
                      ? cloneElement<any>(
                        menuItem, 
                        { onClick: handleClose, className: classes.menuItem }
                      )
                      : null
                )}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  )
}

export default PopMenu
