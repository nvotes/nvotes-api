import * as React from "react"
import { cloneElement, useMemo, useState } from "react"
import { useDispatch } from 'react-redux'
import { Logout, toggleSidebar, useTranslate } from 'react-admin'
import { 
  AppBar as MuiAppBar,
  Toolbar,
  Tooltip,
  IconButton,
  useMediaQuery,
  Theme
} from '@material-ui/core'
import classNames from 'classnames'
import { makeStyles } from '@material-ui/core/styles'
import Slide from '@material-ui/core/Slide'
import AccountCircle from '@material-ui/icons/AccountCircle'
import FeedbackIcon from '@material-ui/icons/Feedback'
import ForumIcon from '@material-ui/icons/Forum'
import AccessTimeIcon from '@material-ui/icons/AccessTime'
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck'
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver'
import EmailIcon from '@material-ui/icons/Email'
import HelpOutline from '@material-ui/icons/HelpOutline'
import TelegramIcon from '@material-ui/icons/Telegram'
import SmsIcon from '@material-ui/icons/Sms'
import NotificationsIcon from '@material-ui/icons/Notifications'
import SettingsIcon from '@material-ui/icons/Settings'
import useScrollTrigger from '@material-ui/core/useScrollTrigger'
import MenuItem from '@material-ui/core/MenuItem'
import PopMenu from '../PopMenu/PopMenu'

const useStyles = makeStyles(
  theme => ({
    appbar: {
      backgroundColor: theme.palette.secondary.contrastText
    },
    toolbar: {
      margin: theme.spacing(3),
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(5),
      transition: 'all .2s ease .01s',
      "&.elevated": {
        margin: theme.spacing(0.5),
      }
    },
    title: {
      flex: 1,
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
    },
    spacer: {
      flex: 1,
    },
    logo: {
      maxWidth: theme.spacing(4)
    },
    popMenuItem: {
      color: theme.palette.secondary.main,
      '& svg': {
        marginRight: theme.spacing(1.25),
        color: theme.palette.secondary.light,
      },
      '&:hover': {
        color: theme.palette.primary.main,
        background: 'inherit',
        '& svg': {
          color: theme.palette.primary.main
        }
      }
    },
    popMenuTopHeader: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
      opacity: "1 !important",
      borderTopLeftRadius: theme.spacing(1.25),
      borderTopRightRadius: theme.spacing(1.25),
      padding: theme.spacing(2),
      marginBottom: theme.spacing(1.25)
    },
    smallMenuItem: {
      '& button': {
        color: theme.palette.secondary.light,
        transition: 'all .2s ease .01s',
        '&:hover': {
          color: theme.palette.primary.main,
          backgroundColor: 'transparent'
        }
      }
    },
    userMenu: {
      '& ul': {
        paddingTop: 0,
      },
      '& button': {
        border: 'solid 2px #b3c0fb',
        padding: '0',
        marginLeft: theme.spacing(2),
        transition: 'all .2s ease .01s',
        borderRadius: '50%',
        '&:hover': {
          borderColor: theme.palette.primary.main
        }
      }
    },
    userMenuIcon: {
      color: theme.palette.secondary.main,
      fontSize: theme.spacing(6) + 'px !important'
    },
    menuButton: {
      marginLeft: '0.5em',
      marginRight: '0.5em',
    },
    menuButtonIconClosed: {
      transition: theme.transitions.create(['transform'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      transform: 'rotate(0deg)',
    },
    menuButtonIconOpen: {
        transition: theme.transitions.create(['transform'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        transform: 'rotate(360deg)',
    }
  }),
  { name: 'NvAppBar' }
)

interface HideElevateProps {
  children: React.ReactElement,
  setElevatedHandler: (elevated: boolean) => void
}

const HideElevate = (props: HideElevateProps) => {
  const { setElevatedHandler } = props
  const hideTrigger = useScrollTrigger({target: undefined})
  const elevationTrigger: boolean = useScrollTrigger({
    disableHysteresis: true,
    threshold: 20
  })

  // notify changes in elevation
  useMemo(() => 
    setElevatedHandler(elevationTrigger), 
    [setElevatedHandler, elevationTrigger]
  )

  return (
    <Slide appear={false} direction="down" in={!hideTrigger}>
      {cloneElement(props.children, {
        elevation: elevationTrigger ? 20 : 0,
      })}
    </Slide>
  )
}

interface AppBarProps {
  open?: boolean
}

/**
 * App navigation bar that shows a shadow when user scroll, giving the 
 * appearance of elevating the navigation bar and thus making it visible that
 * the user has made scroll.
 */
const AppBar = (props: AppBarProps) => {
    const { open } = props
    const classes = useStyles()
    const translate = useTranslate()
    const dispatch = useDispatch()
    const [isElevated, setIsElevated] = useState(false)
    const isXSmall = useMediaQuery<Theme>(theme =>
      theme.breakpoints.down('xs')
    )

    return (
      <HideElevate setElevatedHandler={setIsElevated}>
        <MuiAppBar className={classes.appbar} {...props}>
          <Toolbar
            disableGutters
            variant={isXSmall ? 'regular' : 'dense'}
            className={classNames(classes.toolbar, {"elevated": isElevated})}
          >
            <Tooltip
              title={translate(
                open
                  ? 'ra.action.close_menu'
                  : 'ra.action.open_menu',
                {
                  _: 'Open/Close menu',
                }
              )}
              enterDelay={500}
            >
              <IconButton
                color="inherit"
                onClick={() => dispatch(toggleSidebar())}
                className={classNames(classes.menuButton)}
              >
                <img 
                  src='logo192.png' 
                  alt="nVotes Logo"
                  className={
                    classes.logo + 
                    " " + 
                    (open
                      ? classes.menuButtonIconOpen
                      : classes.menuButtonIconClosed
                    )
                  } 
                />
              </IconButton>
            </Tooltip>               
            <span className={classes.spacer} />

            <PopMenu 
              className={classes.smallMenuItem}
              icon={<NotificationsIcon />}
              label={translate('nvotes.gui.menu.notifications')}
            >
              <div></div>
            </PopMenu>

            <PopMenu 
              className={classes.smallMenuItem}
              icon={<HelpOutline />}
              label={translate('nvotes.gui.menu.help')}
            >
              <div>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <HelpOutline />
                  {translate('nvotes.gui.menu.documentation')}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <RecordVoiceOverIcon />
                  {translate('nvotes.gui.menu.changes')}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <AccessTimeIcon />
                  {translate('nvotes.gui.menu.serviceStatus')}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <PlaylistAddCheckIcon />
                  {translate('nvotes.gui.menu.takeTheTour')}
                </MenuItem>
              </div>
            </PopMenu>

            <PopMenu
              className={classes.smallMenuItem}
              icon={<SmsIcon />}
              label={translate('nvotes.gui.menu.support')}
            >
              <div>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <FeedbackIcon />
                  {translate('nvotes.gui.menu.feedback')}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <ForumIcon />
                  {translate('nvotes.gui.menu.communityForum')}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <TelegramIcon />
                  {translate('nvotes.gui.menu.chatSupport')}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <EmailIcon />
                  {translate('nvotes.gui.menu.contactUs')}
                </MenuItem>
              </div>
            </PopMenu>

            <PopMenu
              className={classes.userMenu}
              icon={<AccountCircle className={classes.userMenuIcon} />}
              label={translate('nvotes.gui.menu.account')}
            >
              <div>
                <MenuItem disabled={true} className={classes.popMenuTopHeader}>
                  {translate(
                    'nvotes.gui.menu.helloAccount',
                    {
                      name: 'Eduardo',
                      primaryId: 'edulix@nvotes.com'
                    }
                  )}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <SettingsIcon />
                  {translate('nvotes.gui.menu.organizationSettings')}
                </MenuItem>
                <MenuItem button={true} className={classes.popMenuItem}>
                  <AccountCircle />
                  {translate('nvotes.gui.menu.myAccount')}
                </MenuItem>
                <Logout button={true} className={classes.popMenuItem}/>
              </div>
            </PopMenu>
            </Toolbar>
        </MuiAppBar>
      </HideElevate>
    )
}

AppBar.defaultProps = {
  open: true
} as Partial<AppBarProps>

export default AppBar