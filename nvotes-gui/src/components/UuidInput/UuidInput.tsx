import * as React from "react"
import {TextInput} from 'react-admin'
import { v4 as uuid_v4 } from 'uuid'

export const UuidRegex = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/

export const shortUuid = (uuid: string) => uuid.split('-')[0]

export const UuidValidator = 
  (message = 'not a valid uuid') => 
    (value: string) => {
      if (UuidRegex.test(value)) {
        return undefined
      } else {
        return message
      }
    }

export const UuidDefaultValue = () => uuid_v4()

export const UuidInput = (props: any) => (
  <TextInput 
    validation={UuidValidator}
    {...props}
  />
)