import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders nLive text', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/nLive/i);
  expect(linkElement).toBeInTheDocument();
});
