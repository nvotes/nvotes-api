import React, {useContext} from "react"
import { Layout } from 'react-admin'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '../AppBar/AppBar'
import { AppContext } from '../../providers/AppContext'

const useStyles = makeStyles(
  theme => ({
    layout: {
      '& main': {
        [theme.breakpoints.up('xs')]: {
          marginTop: theme.spacing(5) + "px !important",
        },
        [theme.breakpoints.down('xs')]: {
          marginTop: theme.spacing(5) + "px !important",
        }
      }
    }
  }),
  {name: 'NvAppLayout'}
)

const AppLayout = (props: any) => {
  const classes = useStyles()
  const appContext = useContext(AppContext)

  return (
    <Layout 
      {...props} 
      title=""
      appBar={AppBar}
      className={classes.layout}
      theme={appContext.theme}
    />
  )
}

export default AppLayout
